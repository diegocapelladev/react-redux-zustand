# Get Started

```sh
npm install

# or

yarn install
```
## Run

```sh
npm run dev

# or

yarn dev
```

## Eslint configured

[@rocketseat/eslint-config](https://www.npmjs.com/package/@rocketseat/eslint-config?activeTab=readme)
